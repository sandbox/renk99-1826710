<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">
<html lang="en" dir="ltr" version="HTML+RDFa 1.1"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
  global $base_url;
  foreach($css as $item) {
    if(preg_match("/http/i",$item))
      print "<link rel=\"stylesheet\" href=\"{$item}\" />\n";
    else
      print "<link rel=\"stylesheet\" href=\"{$base_url}/{$item}\" />\n";
  }
  foreach($js as $item) {
    if(preg_match("/http/i",$item))
      print "<script type=\"text/javascript\" src=\"{$item}\"></script>\n";
    else
      print "<script type=\"text/javascript\" src=\"{$base_url}/{$item}\"></script>\n";
  }
?>
  <script type="text/javascript">
    var base_url = "<?php print $base_url;?>";
    var module_path = "<?php print drupal_get_path("module","player"); ?>";
  </script>
<title>Music Player | Power By Renk</title>
</head>
<body>
<div id="jquery_jplayer_1" class="jp-jplayer"></div>
<div id="jp_container_1" class="jp-audio">
<div class="layout-hd">
  <div class="logo-wraper">
    <div class="logo">
      <a href="/">
        <img alt="ST music" title="ST music" src="<?php print $items['logo'];?>"/>
      </a>
    </div>
  </div>
  <div class="search-bar">
    <form target="_blank" method="get" action="/search/node">
      <span class="ui-watermark-container ui-watermark-input">
        <input type="text" id="search-sug-input" name="keys" autocomplete="off" class="sug-input" size="24" accesskey="s">
      </span>
        <input type="submit" value="" id="search-sug-submit">
    </form>
  </div>
</div>
<div class="layout-bd column2">
  <div class="tab-main">
    <div class="tab-content cfix">
      <div class="tab-page ui-reelList ui-widget fullHeartShow playing" id="page-song">
        <div class="ui-reelList-header ui-state-default">
          <div class="ui-reelList-header-column c0">
            <div class="ui-reelList-checkbox"></div>Song</div>
            <div class="ui-reelList-header-column c1">Artist</div>
            <div class="ui-reelList-header-column c2">Album</div>
            <div class="ui-reelList-header-column c3"></div>
          </div>
        <div class="jp-playlist ui-reelList-viewport" style="height: 414px;">
          <div class="ui-reelList-canvas" style="width: 100%;min-height:214px;height:100%;">
          </div>
        </div>
      </div>
        <div id="myFooter">
          <div class="playlist-length">Total <span>1</span></div></div>
       </div>
  </div>
</div>
<div class="layout-bd column3">
  <div class="album-wrapper">
    <div class="album">
      <a href="javascript:;">
        <img width="135" height="135" src="">
      </a>
      <div class="album-name">
          <a href="javascript:void(0);"></a>
      </div>
      <div class="split"></div>
      <div class="lrc-wrapper" style="height: auto; ">
        <ul id="lrc_list"></ul>
      </div>
    </div>
  </div>
</div>
<div class="layout-ft">
    <div class="jp-audio panel">
			<div class="jp-type-single left-panel">
				<div class="jp-gui jp-interface">
					<ul class="jp-controls play-btn">
						<li class="prev"><a href="javascript:;" hidefocus="true" class="jp-previous wg-button" tabindex="1"><span class="wg-button-inner"></span></a></li>
						<li class="play stop jp-play"><a href="javascript:;" hidefocus="true" class="wg-button" tabindex="1"><span class="wg-button-inner"></span></a></li>
            <li class="play jp-pause" style="display:none;"><a href="javascript:;" hidefocus="true" class="wg-button" tabindex="1">
                <span class="wg-button-inner"></span></a></li>
						<li class="next"><a href="javascript:;" hidefocus="true" class="jp-next wg-button" tabindex="1"><span class="wg-button-inner"></span></a></li>
					</ul>
				</div>
			</div>
      <div class="right-panel">
        <ul class="playmod">
          <li class="random-mode jp-shuffle">
            <a title="Shuffle" hidefocus="true" class="wg-button" style="display:none;">
              <span class="wg-button-inner"></span></a>
          </li>
          <li class="random-mode jp-shuffle-off" >
            <a title="Shuffle off" hidefocus="true" class="wg-button cur"><span class="wg-button-inner"></span></a>
          </li>
          <li class="single-mode jp-repeat">
            <a title="Repeat" hidefocus="true" class="wg-button"><span class="wg-button-inner"></span></a>
          </li>
          <li class="single-mode jp-repeat-off" style="display:none;">
            <a title="Repeat off" hidefocus="true" class="wg-button cur"><span class="wg-button-inner"></span></a>
          </li>
        </ul>
        <div class="volume">
          <a title="Mute" hidefocus="true" class="mute jp-mute wg-button"><span class="wg-button-inner"></span></a>
          <a title="unMute" hidefocus="true" class="mute muted jp-unmute wg-button"><span class="wg-button-inner"></span></a>
            <div class="vol-slider-wrapper">
                <div id="volSlider" class="jp-volume-slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                  <a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 50%; "></a>
                </div>
            </div>
        </div>
      </div>
      <div class="main-panel">
            <div class="title-wrapper">
                <div class="title">
                    <a class="jp-title songname" target="_blank" href="#">No Music Title</a>
                    <span class="split">-</span>
                    <a class="jp-artists artist" target="_blank" href="#">No Artist</a>
                </div>
            </div>
            <div class="pane">
                <div class="time">
                    <span class="jp-current-time curTime">00:00</span>
                    <span class="split">/</span>
                    <span class="jp-duration totalTime">00:00</span>
                </div>
                <div class="progress-wrapper">
                    <div id="progressSlider" class="jp-progress-slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                        <div class="ui-slider-left"></div>
                        <div class="ui-slider-right"></div>
                        <div class="ui-slider-range ui-widget-header ui-slider-range-min" style="width: 0%; "></div>
                        <div class="jp-seek-bar ui-slider-progressbar ui-widget-header" style="width: 0%; "></div>
                        <a class="ui-slider-handle ui-state-default ui-corner-all ui-slider-handle-loading" hidefocus="true" style="left: 0%; "><span></span></a>
                    </div>
                </div>
            </div>
        </div>
			</div>
		</div>
  </div>
</div>
</div>
<body>
</html>
