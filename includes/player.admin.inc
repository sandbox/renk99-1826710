<?php
/**
 * @file
 * Administrative pages for the Player module.
 */

/**
 * Provides the Player settings form.
 */
function player_settings_form($form, &$form_state) {
  $form = array();

  $form['field_artist'] = array(
    '#title' => t('Artist field name'),
    '#type' => 'textfield',
    '#maxlength' => 32,
    '#size' => 20,
    '#default_value' => variable_get('field_artist','field_artist'),
    '#required' => TRUE,
  );

  $form['field_album'] = array(
    '#title' => t('Album field name'),
    '#type' => 'textfield',
    '#maxlength' => 32,
    '#size' => 20,
    '#default_value' => variable_get('field_album','field_album'),
    '#required' => TRUE,
  );
  
  $form['field_album_cover'] = array(
    '#title' => t('Album cover field name'),
    '#type' => 'textfield',
    '#maxlength' => 32,
    '#size' => 20,
    '#default_value' => variable_get('field_album_cover','field_album_cover'),
    '#required' => TRUE,
  );
  
  $form['field_mp3'] = array(
    '#title' => t('MP3 field name'),
    '#type' => 'textfield',
    '#maxlength' => 32,
    '#size' => 20,
    '#default_value' => variable_get('field_mp3','field_mp3'),
    '#required' => TRUE,
  );
  
  $form['field_lrc'] = array(
    '#title' => t('LRC field name'),
    '#type' => 'textfield',
    '#maxlength' => 32,
    '#size' => 20,
    '#default_value' => variable_get('field_lrc','field_lrc'),
    '#required' => TRUE,
  );

  $form['#validate'][] = 'player_settings_form_validate';

  return system_settings_form($form);
}

/**
 * Validation for player_settings_form().
 */
function player_settings_form_validate($form, &$form_state) {
  $field = field_info_field($form_state['values']['field_artist']);
  if(empty($field)) {
  	form_error($form['field_artist'], t("Filed {$form_state['values']['field_artist']} seems does not exist!"));
  } 
  
  $field = field_info_field($form_state['values']['field_album']);
  if(empty($field)) {
  	form_error($form['field_album'], t("Filed {$form_state['values']['field_album']} seems does not exist!"));
  } 
  
  $field = field_info_field($form_state['values']['field_album_cover']);
  if(empty($field)) {
  	form_error($form['field_album_cover'], t("Filed {$form_state['values']['field_album_cover']} seems does not exist!"));
  } 
  
  $field = field_info_field($form_state['values']['field_mp3']);
  if(empty($field)) {
  	form_error($form['field_mp3'], t("Filed {$form_state['values']['field_mp3']} seems does not exist!"));
  } 
  
  $field = field_info_field($form_state['values']['field_lrc']);
  if(empty($field)) {
  	form_error($form['field_lrc'], t("Filed {$form_state['values']['field_lrc']} seems does not exist!"));
  }
}
