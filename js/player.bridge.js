(function($){
  var config = {
    playClass: ".play",
    addClass: ".add"
  };
  $.player = {
    start: function(info){
      var win = null;
      if($.cookie("musicbox_has_open") == 1){
        win = window.open("","music_player");
      }else{
        win = window.open("/player","music_player","height=600,width=800,top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no");
      }
      $.cookie('pdata',info,{path:"/"});
      win.focus();
    }
  };
  $(config.playClass+","+config.addClass).live("click",function(){
    var info = $(this).attr("music-info");
    if($(this).filter(config.addClass).length && $.cookie("musicbox_has_open") == 1){
      $.cookie("pAct","_add",{path:"/"});
    }else{
      $.cookie("pAct","_init",{path:"/"});
    }
    $.player.start(info);
  });
})(jQuery);
