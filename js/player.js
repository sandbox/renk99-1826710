var currentTime = 0;
$(document).ready (function() {
  var myPlayerData;
  var myPlaylist;
  var myPlayer;
  // progress bar & volume bar
  var myControl = {
        progress: $(".progress-wrapper .jp-progress-slider"),
        volume: $(".vol-slider-wrapper .jp-volume-slider")
     };

  // init & start to play
  var setup = function() {
    var pdata = $.cookie("pdata");
    var pAct = $.cookie("pAct");
    $.cookie("pAct",null,{path:"/"});
    if(pdata == null || pAct == null){
      return false;
    }else{
      if(pAct == "_init"){
        getMusicInfo(pdata,_init);
      }else if(pAct == "_add"){
        getMusicInfo(pdata,_add);
      }
    }
  };

  var _updateControls = function(){
    var total = myPlaylist.playlist.length;
    $(".playlist-length span").text(total);
  }

  // init the player
  var _init = function(data) {
    if(myPlaylist != undefined){
      _add(data,true);
    }else{
       myPlaylist = new jPlayerPlaylist({
            jPlayer: "#jquery_jplayer_1",
            cssSelectorAncestor: "#jp_container_1"
            },
            data,
            {
              swfPath: base_url+"/"+module_path+"/js",
              supplied: "mp3",
              wmode: "window",
              timeupdate: function(event) {
                  currentTime = event.jPlayer.status.currentTime;
                  myControl.progress.slider("value", event.jPlayer.status.currentPercentAbsolute);
              },
              volumechange: function(event) {
                if(event.jPlayer.options.muted) {
                  myControl.volume.slider("value", 0);
                } else {
                  myControl.volume.slider("value", event.jPlayer.options.volume);
                }
              }
            }
          );
      // get the player object
      myPlayer = $(myPlaylist.cssSelector.jPlayer);
      myPlayerData = myPlayer.data("jPlayer");
    }
    _updateControls();
  };

  // add songs to playlist
  var _add = function(data,playNow){
    for (i=0;i<data.length;i++){
      if(!_checkExist(data[i].nid)){
        myPlaylist.add(data[i],playNow);
      }else{
        myPlaylist.play(i);
      }
    }
    _updateControls();
  };

  var _checkExist = function(nid) {
    for(j=0;j<myPlaylist.playlist.length;j++){
      if(nid == myPlaylist.playlist[j].nid){
        return true;
      }
    }
    return false;
  };

//   Create the progress slider control
  myControl.progress.slider({
    animate: "fast",
    max: 100,
    range: "min",
    step: 0.1,
    value : 0,
    slide: function(event, ui) {
      var sp = myPlayerData.status.seekPercent;
      if(sp > 0) {
        myPlayer.jPlayer("playHead", ui.value * (100 / sp));
      } else {
        setTimeout(function() {
          myControl.progress.slider("value", 0);
        }, 0);
      }
    }
  });

//   Create the volume slider control
  myControl.volume.slider({
    animate: "fast",
    max: 1,
    range: "min",
    step: 0.01,
    value : $.jPlayer.prototype.options.volume,
    slide: function(event, ui) {
      myPlayer.jPlayer("option", "muted", false);
      myPlayer.jPlayer("option", "volume", ui.value);
    }
  });
  var getMusicInfo = function(data,callback) {
    data = data.split("/");
    var url = "/player/getMusicInfo/"+data[0]+"/"+data[1];
    $.get(url,function(response){
      if(response.status == '00'){
        callback(response.data);
      }else{
        alert("添加歌曲失败");
      }
    });
  };

  var setCookie = function(key,value,time){
    var exdate = new Date();
    var strsec = time*1000;
    exdate.setTime(exdate.getTime() + strsec);
    $.cookie(key,value,{path:"/",expires:exdate});
  };

  setInterval(setup,500);
  // set a flag to indicate the music box has been opened.
  $.cookie("musicbox_has_open",1,{path:"/"});
  // when the music box close, update the flag.
  $(window).bind('beforeunload', function (e) {
    $.cookie("musicbox_has_open",0,{path:"/"});
  });

  $('.jp-gui ul li a').hover(
    function() { $(this).addClass('wg-button-hover'); },
    function() { $(this).removeClass('wg-button-hover'); }
  );

  $('.jp-playlist .options .delete').live("click",function(event){
    var index = $(this).parent().parent().index();
    myPlaylist.remove(index);
    var total = myPlaylist.playlist.length-1;
    $(".playlist-length span").text(total);;
    event.stopPropagation();
  });

});
